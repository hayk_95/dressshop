package com.dress.dressshop.base

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.dress.dressshop.R
import com.dress.dressshop.helpers.Utils
import com.dress.dressshop.views.LoadingDialogFragment


abstract class BaseFragment : Fragment() {

    private val loadingTag = "loading"

    private var loadingDialog: LoadingDialogFragment? = null

    private var pendingLoadingCount = 0


    fun showLoadingDialog() {
        if (pendingLoadingCount == 0) {
            dismissCurrentDialog()
            loadingDialog = LoadingDialogFragment()
            loadingDialog?.show(childFragmentManager, loadingTag)
        }
        pendingLoadingCount++
    }

    fun dismissCurrentDialog() {
        if (childFragmentManager.findFragmentByTag(loadingTag) != null) {
            if (pendingLoadingCount > 0) {
                pendingLoadingCount--
            }
            if (pendingLoadingCount == 0) {
                loadingDialog!!.dismiss()
            }
        }
    }

    fun showErrorDialog(errorMsg: String?) {
        dismissCurrentDialog()
        val errorMessage =
            if (Utils.isNetworkConnected(requireContext())) if (errorMsg != null && errorMsg.isNotEmpty()) errorMsg
            else getString(R.string.default_error_message) else getString(R.string.no_connection_error_message)
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.error)
            .setMessage(errorMessage)
            .setPositiveButton(getString(R.string.ok)) { dialog, which -> dialog.dismiss() }
            .create().show()
    }
}