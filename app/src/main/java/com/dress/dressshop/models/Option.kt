package com.dress.dressshop.models

import com.dress.dressshop.R

data class Option(val name: String, val colorOption: Boolean = false){
    var colorRes: Int? = null
    init {
        if(colorOption){
            colorRes = when(name){
                "Red" -> R.color.red
                "Green" -> R.color.green
                "Blue" -> R.color.blue
                "Gray" -> R.color.gray
                else -> R.color.transparent
            }
        }
    }
}