package com.dress.dressshop.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Dress(
    val id: String,
    val name: String,
    val photo: String,
    val currentPrice: Float,
    val oldPrice: Float?,
    val rating: Float?,
    val ratingCount: Int?,
    val saleTime: Date?,
    var isLiked: Boolean = false
) : Parcelable