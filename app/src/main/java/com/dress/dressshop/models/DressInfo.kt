package com.dress.dressshop.models

data class DressInfo(
    val desc: String,
    val productCode: String,
    val category: String,
    val material: String,
    val country: String,
    val sizes: ArrayList<Option>,
    val colors: ArrayList<Option>
)