package com.dress.dressshop.models

data class BoardPage(val imageId: Int, val title: String?, val desc: String)
