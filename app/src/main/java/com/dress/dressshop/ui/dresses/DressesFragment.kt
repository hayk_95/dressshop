package com.dress.dressshop.ui.dresses

import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.dress.dressshop.general.MainActivity
import com.dress.dressshop.R
import com.dress.dressshop.adapters.DressesAdapter
import com.dress.dressshop.base.BaseFragment
import com.dress.dressshop.data.Status
import com.dress.dressshop.models.Dress
import com.dress.dressshop.viewmodels.DressesViewModel
import kotlinx.android.synthetic.main.fragment_dresses.*


class DressesFragment : BaseFragment() {

    private lateinit var adapter: DressesAdapter
    private val viewModel: DressesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dresses, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        bindData()
    }

    private fun initViews(view: View) {
        dressesList.layoutManager = GridLayoutManager(context, 2)
        val wm = requireContext().getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width: Int = size.x
        adapter = DressesAdapter((width - resources.getDimensionPixelSize(R.dimen.dp30)) / 2)
        dressesList.adapter = adapter

        adapter.setOnDressItemClickListener(object : DressesAdapter.DressItemClickListener {
            override fun onItemClicked(dress: Dress) {
                parentFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.main_container, DressInfoFragment.newInstance(dress)).commit()
            }

            override fun onItemLongClicked(dress: Dress) {
                (activity as MainActivity).addProduct()
            }
        })
    }

    private fun bindData() {
        viewModel.getDresses().observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    dismissCurrentDialog()
                    resource.data?.let { dresses ->
                        adapter.updateDresses(dresses)
                    }
                }
                Status.ERROR -> {
                    showErrorDialog(resource.message)
                }
                Status.LOADING -> {
                    showLoadingDialog()
                }
            }
        })
    }
}