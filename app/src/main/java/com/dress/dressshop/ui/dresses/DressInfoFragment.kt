package com.dress.dressshop.ui.dresses

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.dress.dressshop.general.MainActivity
import com.dress.dressshop.R
import com.dress.dressshop.base.BaseFragment
import com.dress.dressshop.data.Status
import com.dress.dressshop.models.Dress
import com.dress.dressshop.models.Option
import com.dress.dressshop.viewmodels.DressInfoViewModel
import kotlinx.android.synthetic.main.fragment_dress_info.*

private const val DRESS_PARAM = "dressParam"

class DressInfoFragment : BaseFragment() {
    private var dress: Dress? = null
    private val viewModel: DressInfoViewModel by viewModels()

    companion object {
        @JvmStatic
        fun newInstance(dress: Dress) =
            DressInfoFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(DRESS_PARAM, dress)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dress = arguments?.getParcelable(DRESS_PARAM)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dress_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        bindData()
        setListeners()
    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    @SuppressLint("SetTextI18n")
    private fun initViews() {
        name.text = dress?.name
        currentPrice.text = "$ " + dress?.currentPrice
        oldPrice.paintFlags = oldPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        oldPrice.text = if (dress?.oldPrice != null) "$ " + dress?.oldPrice else ""
        if (dress?.rating != null) {
            rating.rating = dress?.rating!!
            ratingCount.text = "(" + dress?.ratingCount!! + ")"
        } else {
            rating.visibility = View.GONE
            ratingCount.visibility = View.GONE
        }
        category.paintFlags = category.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        sizeOption.setOptionName(getString(R.string.size))
        colorOption.setOptionName(getString(R.string.color))
        val list = ArrayList<Option>()
        for (i in 1 until 10) {
            list.add(Option(i.toString()))
        }
        countOption.addOptionsList(list)
        Glide.with(photo).load(dress?.photo).placeholder(R.drawable.placeholder).into(photo)
    }

    private fun bindData() {
        dress?.let {
            viewModel.getDressInfo(it.id).observe(viewLifecycleOwner, Observer { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        dismissCurrentDialog()
                        resource.data?.let { dressInfo ->
                            description.text = dressInfo.desc
                            productCode.text = dressInfo.productCode
                            category.text = dressInfo.category
                            material.text = dressInfo.material
                            country.text = dressInfo.country
                            sizeOption.addOptionsList(dressInfo.sizes)
                            colorOption.addOptionsList(dressInfo.colors)
                        }
                    }
                    Status.ERROR -> {
                        showErrorDialog(resource.message)
                    }
                    Status.LOADING -> {
                        showLoadingDialog()
                    }
                }
            })
        }
    }

    private fun setListeners() {
        descLayout.setOnClickListener {
            arrowIcon.setImageDrawable(
                resources.getDrawable(
                    if (description.visibility != View.VISIBLE)
                        R.drawable.arrow_up else R.drawable.arrow_down, null
                )
            )

            description.visibility =
                if (description.visibility != View.VISIBLE) View.VISIBLE else View.GONE
        }

        close.setOnClickListener { parentFragmentManager.popBackStack() }

        addToCart.setOnClickListener {
            (activity as MainActivity).addProduct()
        }
    }
}