package com.dress.dressshop.ui.onboarding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dress.dressshop.R

class OnboardingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        supportFragmentManager.beginTransaction()
            .replace(R.id.onboarding_container, OnboardingFragment()).commit()
    }
}