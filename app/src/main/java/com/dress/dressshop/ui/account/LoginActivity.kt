package com.dress.dressshop.ui.account

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dress.dressshop.R

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportFragmentManager.beginTransaction().replace(R.id.login_container, LoginFragment()).commit()
    }
}