package com.dress.dressshop.ui.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.dress.dressshop.general.MainActivity
import com.dress.dressshop.R
import kotlinx.android.synthetic.main.fragment_board_page.*

private const val IMAGE_ID_PARAM = "imageIdParam"
private const val TITLE_PARAM = "titleParam"
private const val DESC_PARAM = "descParam"
private const val LAST_SCREEN_PARAM = "lastScreenParam"

class BoardPageFragment : Fragment() {

    companion object {
        @JvmStatic
        fun newInstance(imageId: Int, title: String?, desc: String, lastScreen: Boolean = false) =
            BoardPageFragment().apply {
                arguments = Bundle().apply {
                    putInt(IMAGE_ID_PARAM, imageId)
                    putString(TITLE_PARAM, title)
                    putString(DESC_PARAM, desc)
                    putBoolean(LAST_SCREEN_PARAM, lastScreen)
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_board_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            view.findViewById<ImageView>(R.id.page_image).setImageDrawable(resources.getDrawable(it.getInt(
                IMAGE_ID_PARAM)?:0,null))
            view.findViewById<TextView>(R.id.title).text = it.getString(TITLE_PARAM)
            view.findViewById<TextView>(R.id.desc).text = it.getString(DESC_PARAM)
            view.findViewById<LinearLayout>(R.id.login_layout).visibility = if(it.getBoolean(
                    LAST_SCREEN_PARAM)) View.VISIBLE else View.GONE
        }

        signIn.setOnClickListener{
            activity?.setResult(MainActivity.openLogin)
            activity?.finish()
        }

        signUp.setOnClickListener{
            activity?.setResult(MainActivity.openLogin)
            activity?.finish()
        }
    }
}