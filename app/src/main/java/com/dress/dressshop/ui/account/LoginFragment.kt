package com.dress.dressshop.ui.account

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.dress.dressshop.R
import com.dress.dressshop.base.BaseFragment
import com.dress.dressshop.data.Resource
import com.dress.dressshop.data.Status
import com.dress.dressshop.viewmodels.DressesViewModel
import com.dress.dressshop.viewmodels.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment() {

    private val viewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val observer = Observer<Resource<Boolean>> {
            when (it.status) {
                Status.SUCCESS -> {
                    dismissCurrentDialog()
                    AlertDialog.Builder(requireContext())
                        .setTitle(R.string.congratulations)
                        .setMessage(R.string.successfully_login)
                        .setPositiveButton(R.string.ok)
                        { p0, p1 ->
                            activity?.finish()
                        }.create().show()
                }
                Status.ERROR -> showErrorDialog(it.message)
                Status.LOADING -> showLoadingDialog()
            }
        }
        signIn.setOnClickListener {
            viewModel.signIn(username.text.toString(), password.text.toString()).observe(viewLifecycleOwner, observer)
        }
        signUp.setOnClickListener {
            viewModel.signUp(username.text.toString(), password.text.toString()).observe(viewLifecycleOwner, observer)
        }
    }
}