package com.dress.dressshop.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.dress.dressshop.general.MainActivity
import com.dress.dressshop.R
import com.dress.dressshop.models.BoardPage
import com.dress.dressshop.views.BoardTab
import kotlinx.android.synthetic.main.fragment_onboarding.*

class OnboardingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_onboarding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
    }

    private fun initViews() {
        fragmentsPager.adapter = FragmentsAdapter(
            childFragmentManager, FragmentPagerAdapter.POSITION_NONE,
            listOf(
                BoardPage(
                    R.drawable.delivery_img,
                    getString(R.string.page_title_1),
                    getString(R.string.page_desc)
                ),
                BoardPage(
                    R.drawable.order_img,
                    getString(R.string.page_title_2),
                    getString(R.string.page_desc)
                ),
                BoardPage(
                    R.drawable.uploading_img,
                    null,
                    getString(R.string.page_desc)
                )
            )
        )
        for (i in 0 until (fragmentsPager.adapter as FragmentsAdapter).count) {
            tabsLayout.addView(BoardTab(requireContext()))
        }
        (tabsLayout.getChildAt(0) as BoardTab).setTabSelected(true)
    }

    private fun setListeners() {
        fragmentsPager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                (tabsLayout.getChildAt(position) as BoardTab).setTabSelected(true)
                if (position < tabsLayout.childCount - 1) {
                    (tabsLayout.getChildAt(position + 1) as BoardTab).setTabSelected(false)
                    skip.visibility = View.GONE
                } else {
                    skip.visibility = View.VISIBLE
                }
            }
        })

        skip.setOnClickListener {
            activity?.setResult(MainActivity.skipOnboarding)
            activity?.finish()
        }
    }

    class FragmentsAdapter(fm: FragmentManager, behavior: Int, private val pages: List<BoardPage>) :
        FragmentPagerAdapter(fm, behavior) {

        override fun getItem(position: Int): Fragment {
            with(pages[position]) {
                return BoardPageFragment.newInstance(
                    imageId,
                    title,
                    desc,
                    position == pages.size - 1
                )
            }
        }

        override fun getCount(): Int = pages.size

    }
}