package com.dress.dressshop.general

import android.app.Application

class AppDelegate : Application() {

    companion object{
        lateinit var instance: AppDelegate
    }
    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}