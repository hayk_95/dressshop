package com.dress.dressshop.general

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.dress.dressshop.helpers.Constants
import com.dress.dressshop.R
import com.dress.dressshop.data.PrefManager
import com.dress.dressshop.ui.account.LoginActivity
import com.dress.dressshop.ui.dresses.DressesFragment
import com.dress.dressshop.ui.onboarding.OnboardingActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*

private const val ONBOARDING_REQUEST_CODE = 101

class MainActivity : AppCompatActivity() {
    companion object{
        const val skipOnboarding = 201
        const val openLogin = 202
    }
    private val auth: FirebaseAuth = Firebase.auth

    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        if(PrefManager.getBoolean(Constants.ONBOARDING_SHOW_KEY, true)){
            startActivityForResult(Intent(this,OnboardingActivity::class.java),
                ONBOARDING_REQUEST_CODE
            )
        } else openDressesFragment()

        profile.setOnClickListener{
            if(auth.currentUser != null){
                Toast.makeText(this,
                    R.string.successfully_login, Toast.LENGTH_SHORT).show()
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dresses_menu, menu);
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == ONBOARDING_REQUEST_CODE){
            PrefManager.setBoolean(Constants.ONBOARDING_SHOW_KEY, false)
            if(resultCode == skipOnboarding){
                openDressesFragment()
            } else if(resultCode == openLogin){
                openDressesFragment()
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
    }

    private fun openDressesFragment(){
        supportFragmentManager.beginTransaction().replace(R.id.main_container, DressesFragment()).commit()
    }

    fun addProduct(){
        if(shopCount.visibility == View.GONE){
            shopCount.visibility = View.VISIBLE
            shopCount.text = "1"
        } else {
            var count = shopCount.text.toString().toInt()
            count++
            shopCount.text = count.toString()
        }
    }
}