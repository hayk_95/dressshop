package com.dress.dressshop.adapters

import android.annotation.SuppressLint
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dress.dressshop.R
import com.dress.dressshop.models.Dress
import org.joda.time.Period


class DressesAdapter(private val holderWidth: Int) : RecyclerView.Adapter<DressesAdapter.DressHolder>() {

    private var dresses: ArrayList<Dress> = ArrayList()
    private var dressItemClickListener: DressItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DressHolder {
        val holder = DressHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.dress_card_layout, parent, false)
        )
        holder.itemView.layoutParams.width = holderWidth
        holder.oldPrice.paintFlags = holder.oldPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        return holder
    }

    override fun getItemCount(): Int {
        return dresses.size
    }

    override fun onBindViewHolder(holder: DressHolder, position: Int) {
        holder.initData(holder, dresses[position])
        holder.setListeners(holder, dresses[position], dressItemClickListener)
    }

    fun updateDresses(dresses: List<Dress>) {
        this.dresses.clear()
        this.dresses.addAll(dresses)
        notifyDataSetChanged()
    }

    fun setOnDressItemClickListener(listener: DressItemClickListener){
        dressItemClickListener = listener
    }

    interface DressItemClickListener {
        fun onItemClicked(dress: Dress)
        fun onItemLongClicked(dress: Dress)
    }

    class DressHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.dress_name)
        var photo: ImageView = itemView.findViewById(R.id.dress_photo)
        var currentPrice: TextView = itemView.findViewById(R.id.dress_current_price)
        var oldPrice: TextView = itemView.findViewById(R.id.dress_old_price)
        var saleTime: TextView = itemView.findViewById(R.id.sale_time)
        var ratingBar: RatingBar = itemView.findViewById(R.id.dress_rating)
        var ratingCount: TextView = itemView.findViewById(R.id.dress_rating_count)
        var like: ImageView = itemView.findViewById(R.id.like)

        @SuppressLint("SetTextI18n")
        fun initData(holder: DressHolder, dress: Dress){
            holder.name.text = dress.name
            holder.currentPrice.text = "$ " + dress.currentPrice.toString()
            if (dress.oldPrice != null) {
                holder.oldPrice.text = "$ " + dress.oldPrice.toString()
            } else holder.oldPrice.text = ""
            if (dress.rating != null) {
                holder.ratingBar.visibility = View.VISIBLE
                holder.ratingCount.visibility = View.VISIBLE
                holder.ratingBar.rating = dress.rating
                holder.ratingCount.text = "(" + dress.ratingCount?.toString() + ")"
            } else {
                holder.ratingBar.visibility = View.GONE
                holder.ratingCount.visibility = View.GONE
            }
            if (dress.saleTime?.time != null) {
                val period = Period(System.currentTimeMillis(), dress.saleTime.time)
                holder.saleTime.text =
                    "Remain: " + period.days + " days " + period.hours + "h:" + period.minutes + "m"
            } else holder.saleTime.text = ""
            Glide.with(holder.itemView).load(dress.photo)
                .placeholder(R.drawable.placeholder).into(holder.photo)
            holder.like.setImageDrawable(
                holder.itemView.resources.getDrawable(
                    if (dress.isLiked) R.drawable.full_like
                    else R.drawable.empty_like, null
                )
            )
        }

        fun setListeners(holder: DressHolder, dress: Dress, onItemClickListener: DressItemClickListener?){
            holder.like.setOnClickListener {
                val isLiked = dress.isLiked
                (it as ImageView).setImageDrawable(
                    holder.itemView.resources.getDrawable(
                        if (!isLiked) R.drawable.full_like
                        else R.drawable.empty_like, null
                    )
                )
                dress.isLiked = !isLiked
            }

            holder.itemView.setOnClickListener { onItemClickListener?.onItemClicked(dress) }
            holder.itemView.setOnLongClickListener {
                onItemClickListener?.onItemLongClicked(dress)
                true
            }
        }
    }
}