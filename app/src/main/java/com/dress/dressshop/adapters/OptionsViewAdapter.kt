package com.dress.dressshop.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.dress.dressshop.R
import com.dress.dressshop.models.Dress
import com.dress.dressshop.models.Option

class OptionsViewAdapter(var options: ArrayList<Option>) : RecyclerView.Adapter<OptionsViewAdapter.OptionHolder>() {

    private var onItemClickListener: ((Option) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionHolder {
        return OptionHolder(LayoutInflater.from(parent.context).inflate(R.layout.option_view_card_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return options.size
    }

    override fun onBindViewHolder(holder: OptionHolder, position: Int) {
        holder.name.text = options[position].name
        if(options[position].colorRes != null) {
            holder.icon.visibility = View.VISIBLE
            holder.icon.setBackgroundColor(
                holder.itemView.resources.getColor(
                    options[position].colorRes!!,
                    null
                )
            )
        }

        holder.itemView.setOnClickListener{onItemClickListener?.invoke(options[position])}
    }

    fun setOnOptionsItemClickListener(listener: (Option) -> Unit){
        onItemClickListener = listener
    }

    class OptionHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var name: TextView = itemView.findViewById(R.id.option_name)
        var icon: ImageView = itemView.findViewById(R.id.option_icon)
    }
}