package com.dress.dressshop.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dress.dressshop.data.DressResource
import com.dress.dressshop.data.Resource
import com.dress.dressshop.models.DressInfo

class DressInfoViewModel : ViewModel() {

    private val resource = DressResource()
    private var dressInfo: MutableLiveData<Resource<DressInfo>>? = null

    fun getDressInfo(dressId: String): LiveData<Resource<DressInfo>> {
        if(dressInfo == null) dressInfo = resource.getDressAdditionalInfo(dressId)
        return dressInfo!!
    }
}