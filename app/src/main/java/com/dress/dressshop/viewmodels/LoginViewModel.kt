package com.dress.dressshop.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dress.dressshop.general.AppDelegate
import com.dress.dressshop.R
import com.dress.dressshop.helpers.Utils
import com.dress.dressshop.data.LoginRepository
import com.dress.dressshop.data.Resource

class LoginViewModel : ViewModel() {
    private val repository = LoginRepository()

    fun signIn(email: String, password: String) : LiveData<Resource<Boolean>>{
        if(!Utils.isValidEmail(email)){
            return MutableLiveData(Resource.error(AppDelegate.instance.getString(R.string.not_valid_email)))
        }
        if(password.length < 8){
            return MutableLiveData(Resource.error(AppDelegate.instance.getString(R.string.not_valid_password)))
        }
        return repository.signIn(email, password)
    }

    fun signUp(email: String, password: String) : LiveData<Resource<Boolean>>{
        if(!Utils.isValidEmail(email)){
            return MutableLiveData(Resource.error(AppDelegate.instance.getString(R.string.not_valid_email)))
        }
        if(password.length < 8){
            return MutableLiveData(Resource.error(AppDelegate.instance.getString(R.string.not_valid_password)))
        }
        return repository.signUp(email, password)
    }
}