package com.dress.dressshop.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dress.dressshop.data.DressResource
import com.dress.dressshop.data.Resource
import com.dress.dressshop.models.Dress

class DressesViewModel : ViewModel() {

    private val resource = DressResource()
    private var dresses: MutableLiveData<Resource<List<Dress>>>? = null

    fun getDresses(): LiveData<Resource<List<Dress>>> {
        if(dresses == null){
            dresses = resource.getDresses()
        }
        return dresses!!
    }
}