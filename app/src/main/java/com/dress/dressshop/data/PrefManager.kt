package com.dress.dressshop.data

import androidx.preference.PreferenceManager

import com.dress.dressshop.general.AppDelegate

object PrefManager {
    fun getInt(key: String, defaultValue: Int = 0) : Int =
        PreferenceManager.getDefaultSharedPreferences(AppDelegate.instance).getInt(key, defaultValue)

    fun setBoolean(key: String, value: Boolean) =
        PreferenceManager.getDefaultSharedPreferences(AppDelegate.instance).edit().putBoolean(key, value).commit()

    fun getBoolean(key: String, defaultValue: Boolean = false) : Boolean =
        PreferenceManager.getDefaultSharedPreferences(AppDelegate.instance).getBoolean(key, defaultValue)
}