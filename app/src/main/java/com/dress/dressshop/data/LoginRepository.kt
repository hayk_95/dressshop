package com.dress.dressshop.data

import androidx.lifecycle.MutableLiveData
import com.dress.dressshop.models.DressInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginRepository {
    private val auth: FirebaseAuth = Firebase.auth

    fun signIn(email: String, password: String): MutableLiveData<Resource<Boolean>> {
        val liveData = MutableLiveData<Resource<Boolean>>(Resource.loading())
        auth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                liveData.postValue(Resource.success(true))
            }
            .addOnFailureListener { liveData.postValue(Resource.error(it.message)) }
        return liveData
    }

    fun signUp(email: String, password: String): MutableLiveData<Resource<Boolean>> {
        val liveData = MutableLiveData<Resource<Boolean>>(Resource.loading())
        auth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                liveData.postValue(Resource.success(true))
            }
            .addOnFailureListener { liveData.postValue(Resource.error(it.message)) }
        return liveData
    }
}