package com.dress.dressshop.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.dress.dressshop.models.Dress
import com.dress.dressshop.models.DressInfo
import com.dress.dressshop.models.Option
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class DressResource {
    private val database = Firebase.firestore

    fun getDresses() : MutableLiveData<Resource<List<Dress>>>{
        val liveData = MutableLiveData<Resource<List<Dress>>>(Resource.loading())
        database.collection("dresses")
            .get()
            .addOnSuccessListener {
                if(it != null && it.documents.isNotEmpty()) {
                    val list: ArrayList<Dress> = ArrayList()
                    for (document in it.documents) {
                        with(document) {
                            list.add(
                                Dress(
                                    id,
                                    get("name") as String,
                                    get("photo") as String,
                                    getDouble("currentPrice")?.toFloat() ?: 0.0f,
                                    getDouble("oldPrice")?.toFloat(),
                                    getDouble("rating")?.toFloat(),
                                    getLong("ratingCount")?.toInt(),
                                    getTimestamp("saleTime")?.toDate()
                                )
                            )
                        }
                    }
                    liveData.postValue(Resource.success(list))
                }else {
                    liveData.postValue(Resource.error())
                }
            }.addOnFailureListener { liveData.postValue(Resource.error(it.message)) }
        return liveData
    }

    fun getDressAdditionalInfo(dressId: String): MutableLiveData<Resource<DressInfo>> {
        val liveData = MutableLiveData<Resource<DressInfo>>(Resource.loading())
        database.collection("dressesInfo").whereEqualTo("dressId", dressId)
            .get()
            .addOnSuccessListener {
                if (it != null && !it.isEmpty && it.documents[0] != null) {
                    Log.d("DBG", "data" + it.documents)
                    val sizes: ArrayList<Option> = ArrayList()
                    val colors: ArrayList<Option> = ArrayList()
                    for (i in it.documents[0].get("sizes") as List<String>) {
                        sizes.add(Option(i))
                    }
                    for (i in it.documents[0].get("colors") as List<String>) {
                        colors.add(Option(i, true))
                    }
                    liveData.postValue(
                        Resource.success(
                            DressInfo(
                                it.documents[0].get("desc") as String,
                                it.documents[0].get("productCode") as String,
                                it.documents[0].get("category") as String,
                                it.documents[0].get("material") as String,
                                it.documents[0].get("country") as String,
                                sizes,
                                colors
                            )
                        )
                    )
                } else {
                    liveData.postValue(Resource.error())
                }
            }.addOnFailureListener {
                liveData.postValue(Resource.error(it.message))
            }
        return liveData
    }
}