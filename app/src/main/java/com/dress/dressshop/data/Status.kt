package com.dress.dressshop.data

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}