package com.dress.dressshop.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dress.dressshop.R
import com.dress.dressshop.adapters.OptionsViewAdapter
import com.dress.dressshop.models.Option
import kotlinx.android.synthetic.main.options_view_layout.view.*

class OptionsView(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
    ConstraintLayout(context, attrs, defStyleAttr) {

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context) : this(context, null, 0)

    private var optionName: TextView
    private var currentOption: TextView
    private var currentIcon: ImageView
    private var optionList: RecyclerView

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.options_view_layout, this, true)
        optionName = view.findViewById(R.id.option_name)
        currentOption = view.findViewById(R.id.current_option_name)
        currentIcon = view.findViewById(R.id.current_option_icon)
        optionList = view.findViewById(R.id.options_list)
        optionList.layoutManager = LinearLayoutManager(context)

        view.setOnClickListener{
            optionArrowIcon.setImageDrawable(resources.getDrawable(if(optionList.visibility != View.VISIBLE)
            R.drawable.arrow_up else R.drawable.arrow_down, null))
            optionList.visibility = if(optionList.visibility != View.VISIBLE) View.VISIBLE else View.GONE
        }
    }

    fun setOptionName(name: String){
        optionName.text = name
    }

    fun addOptionsList(options: ArrayList<Option>){
        val adapter = OptionsViewAdapter(options)
        optionList.adapter = adapter
        currentOption.text = options[0].name
        if(options[0].colorOption && options[0].colorRes != null)
            currentIcon.setBackgroundColor(resources.getColor(options[0].colorRes!!, null))

        adapter.setOnOptionsItemClickListener {
            currentOption.text = it.name
            if(it.colorOption && it.colorRes != null)
                currentIcon.setBackgroundColor(resources.getColor(it.colorRes!!, null))
        }
    }
}